package com.lincoln.bean;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 接口通用异常
 *
 * @author lincoln
 */
@Getter
@Setter
@AllArgsConstructor
public class ApiCodeException extends Exception {
    protected static final Logger logger = LoggerFactory.getLogger(ApiCodeException.class);
    private static final int FAIL = -1;

    ApiCode apiCode;

    public ApiCodeException() {
        this.apiCode = ApiCode.findCode(FAIL);
        log();
    }

    private void log() {
        logger.error(apiCode.getMsg());
    }

    public ApiCodeException(int apicode) {
        this.apiCode = ApiCode.findCode(apicode);
        if (apiCode == null) {
            this.apiCode = ApiCode.findCode(FAIL);
        }
        log();
    }

    public ApiCodeException(int apicode, String msg) {
        this.apiCode = ApiCode.findCode(apicode);
        if (apiCode == null) {
            this.apiCode = ApiCode.findCode(FAIL);
        }
        this.apiCode.setMsg(msg);
        log();
    }

    public ApiCode getApiCode() {
        return apiCode;
    }

    @Override
    public String toString() {
        return apiCode.getMsg();
    }
}
