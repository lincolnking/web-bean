package com.lincoln.config;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

/**
 * 启动服务器前执行
 *
 * @author lincoln
 */
public class StartRun implements CommandLineRunner, ApplicationListener<ContextRefreshedEvent> {
    private static ApplicationContext applicationContext = null;

    @Override
    public void run(String... args) throws Exception {
        String remark = "要用到这个配置,请在你的项目中继承这个类,并在类上注解@Component和@Order(value = 1),再重写这个方法";
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        if (applicationContext == null) {
            applicationContext = contextRefreshedEvent.getApplicationContext();
        }
    }

    public static ApplicationContext getApplicationContext() {
        return applicationContext;
    }
}
